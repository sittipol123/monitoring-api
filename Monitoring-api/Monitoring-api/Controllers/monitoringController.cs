﻿using Monitoring_api.Database;
using Monitoring_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Http.Cors;

namespace Monitoring_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class monitoringController : ApiController
    {
        #region GET

        public object PostListByStatus(int pageSize=0, int pageNumber=0,string groupStatus ="",string status="", string orderBy = "")
        {
            MonitoringDataSourceDataContext db = new MonitoringDataSourceDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);
            IQueryable<VW_TERMINAL_CURRENT_STATUS> query = db.VW_TERMINAL_CURRENT_STATUS.OrderBy(x => x.FLAG_LAST_UPDATE);

            byte statusByte;
            if (string.IsNullOrEmpty(status) && byte.TryParse(status, out statusByte))
            {
                query = query.Where(x => x.STATUS == statusByte);
            }

            switch (groupStatus.ToLower())
            {
                case "all":
                    //NOT Any thing.
                    break;
            }
            int totalCount = query.Count();

            //var urlHelper = new UrlHelper(Request);
            //var prevLink = pageSize > 0 ? urlHelper.Link("monitoring", new { page = pageNumber - 1 }) : "";

            //var nextLink = pageSize!=0 && pageSize < totalCount - 1 ? urlHelper.Link("monitoring", new { page = pageNumber + 1 }) : "";
          
            var totalPages = pageSize!=0?(int)Math.Ceiling((double)totalCount / pageSize):0;
            List<VW_TERMINAL_CURRENT_STATUS> list;
            if (pageSize == 0)
            {
                list = query.ToList();
            }
            else {
                list = query.Skip(pageNumber * pageSize).Take(pageSize).ToList();
            }
            
            return new 
            {
                TotalCount = totalCount,
                TotalPages = totalPages,
                //PrevPageLink = prevLink,
                //NextPageLink = nextLink,
                Results = list
            };
        }
        #endregion
    }
}
