﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Monitoring_api.Database;
namespace Monitoring_api.Models
{
    public class MonitoringModel
    {
        public List<VW_TERMINAL_CURRENT_STATUS> Results;
        public int TotalCount;
        public int TotalPages;
        public string PrevPageLink;
        public string NextPageLink;
    }
}